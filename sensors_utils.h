/**
 * @file
 * @author Martin Stejskal
 * @brief Utilities for "sensors" module
 */
#ifndef __SENSORS_UTILS_H__
#define __SENSORS_UTILS_H__
// ===============================| Includes |================================
#include "sensors_common.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
/**
 * @brief List of supported modes
 */
typedef enum {
  SENSOR_UTIL_MODE_RAW_SAMPLES,
  SENSOR_UTIL_MODE_TAP_DETECTION,
  SENSOR_UTIL_MODE_ORIENTATION_TRACKING,

  SENSOR_UTIL_MODE_MAX
} te_sensor_util_mode;

// ==================| Structures, enumerations - Sampling |==================
typedef void (*tf_sensor_utils_get_data_cb)(ts_sensor_utils_data *ps_data);

typedef struct {
  // One of the selected feature
  te_sensor_feature e_feature;

  // Typically it is not possible to get better data rate than 1kHz anyway
  uint32_t u32_sampling_period_ms;

  // Callback function which pass loaded data
  tf_sensor_utils_get_data_cb pf_get_data;
} ts_sensor_util_sampling_args;

// ===============| Structures, enumerations - tap detection |================
typedef void (*tf_sensor_stil_tap_detected_cb)(void);

typedef struct {
  // Minimum vector length - bottom threshold for shock detection. Any value
  // below will not be considered as shock
  uint16_t u16_min_vector_length;

  // Maximum vector length - top threshold for shock detection. Any bigger value
  // will not be considered as valid shock
  uint16_t u16_max_vector_length;

  // Minimum shock duration in milliseconds
  uint16_t u16_min_duration_ms;
  // Maximum shock duration in milliseconds
  uint16_t u16_max_duration_ms;

  // When shock above maximum threshold is detected, do not detect anything
  // for this time period
  uint16_t u16_hold_off_ms;
} ts_sensor_util_tap_det_params;

typedef struct {
  // One of the selected feature
  te_sensor_feature e_feature;

  // Typically it is not possible to get better data rate than 1kHz anyway
  uint32_t u32_sampling_period_ms;

  // Callback executed when tap is detected
  tf_sensor_stil_tap_detected_cb pf_tap_detected;

  // Parameter for core tap detection algorithm
  ts_sensor_util_tap_det_params s_params;
} ts_sensor_util_tap_det_args;

// ============| Structures, enumerations - orientation tracking |============
typedef void (*tf_sensor_util_ori_det_cb)(bool b_in_position);

typedef struct {
  // Target Theta angle in degrees
  int16_t i16_target_theta_deg;
  // Target Phi angle in degrees
  int16_t i16_target_phi_deg;

  // Absolute tolerance for Theta angle when estimating target position
  int16_t i16_tolerance_theta_deg;
  // Absolute tolerance for Phi angle when estimating target position
  int16_t i16_tolerance_phi_deg;

  // In case that Theta is close to 0 or 180 degrees, Phi angle can quite
  // oscillate and actually does not have much meaning in such cases. So in real
  // life, we might want to ignore Phi angle if approaching those two values.
  // This value tells how much Theta have to be far from those two angles in
  // order to take Phi into account when estimating target position
  uint16_t u16_ignore_phi_tolerance_deg;
} ts_sensor_util_ori_det_params;

typedef struct {
  // One of the selected feature
  te_sensor_feature e_feature;

  // Typically it is not possible to get better data rate than 1kHz anyway
  uint32_t u32_sampling_period_ms;

  // Executed when device goes or leaves predefined position
  tf_sensor_util_ori_det_cb pf_ori_changed;

  // Parameters for core orientation detection algorithm
  ts_sensor_util_ori_det_params s_params;
} ts_sensor_util_ori_det_args;
// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
/**
 * @brief High level application that handle requests
 *
 * This task have to be executed before any requests are called. Otherwise it
 * can not work.
 *
 * @param pv_args Pointer to arguments. Can be empty. Actually not used.
 */
void sensors_utils_task(void *pv_args);

/**
 * @brief Check if task was initialized or not
 *
 * Once task is initialized, it is possible to send requests
 *
 * @return True if already initialized, false otherwise
 */
bool sensors_utils_is_initialized(void);
// ========================| Middle level functions |=========================
/**
 * @brief Start sampling raw data from required feature
 *
 * @note In order to allow high performance, data are simply read in raw
 *        format without any recalculation.
 *
 * @param s_args Input arguments
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         available in current sensor set
 */
e_sensor_error sensors_utils_sampling_start(
    const ts_sensor_util_sampling_args s_args);

/**
 * @brief Stop sampling raw data
 * @return SENSOR_OK if no error
 */
e_sensor_error sensors_utils_sampling_stop(void);

/**
 * @brief Start tap detection for required feature
 *
 * @note Tap detection require relatively fast processing, therefore data
 *       are internally read in raw format in order to not loose much
 *       performance.
 *
 * @param s_args Input arguments
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         available in current sensor set
 */
e_sensor_error sensors_utils_tap_detection_start(
    const ts_sensor_util_tap_det_args s_args);

/**
 * @brief Stop tap detection mode
 * @return SENSOR_OK if no error
 */
e_sensor_error sensors_utils_tap_detection_stop(void);

/**
 * @brief Start orientation tracking for required feature
 * @param s_args Input arguments
 * @return SENSOR_OK if no error. SENSOR_NOT_SUPPORTED if feature is not
 *         available in current sensor set
 */
e_sensor_error sensors_utils_ori_tracking_start(
    const ts_sensor_util_ori_det_args s_args);

/**
 * @brief Stop orientation tracking
 * @return SENSOR_OK if no error
 */
e_sensor_error sensors_utils_ori_tracking_stop(void);

/**
 * @brief Returns last measured angles Thetha and Phi
 *
 * If running in "orientation tracking" mode, last calculated angles Thetha and
 * Phi are stored in memory, so upper application can use them for debug or
 * inform user.
 *
 * @param[out] pi16_theta_deg Theta will be stored here. Value is in degrees
 * @param[out] pi16_phi_deg Phi will be stored here. Value is in degrees
 * @return SENSOR_OK if no error, SENSOR_NOT_INITIALIZED if logic is not
 *         initialized at all, SENSOR_EMPTY_POINTER if any pointer is empty,
 *         SENSOR_FAIL otherwise
 */
e_sensor_error sensors_utils_ori_last_angles(int16_t *pi16_theta_deg,
                                             int16_t *pi16_phi_deg);
// ==========================| Low level functions |==========================
/**
 * @brief Calculate Theta and Phi to get spherical orientation coordinates
 *
 * Theta and Phi angles can be used for recognizing device orientation in
 * 3D space. Note that those angles are spherical, not "2D".
 *
 * @param[in] ps_data Input data. Have to be vector type
 * @param[out] pi16_theta_deg Theta will be stored here. Value is in degrees
 * @param[out] pi16_phi_deg Phi will be stored here. Value is in degrees
 * @return
 */
e_sensor_error sensors_utils_get_ori_angles(const ts_sensor_utils_data *ps_data,
                                            int16_t *pi16_theta_deg,
                                            int16_t *pi16_phi_deg);

/**
 * @brief Check if device is in required position or not
 *
 * @param i16_theta_deg Theta angle in degrees
 * @param i16_phi_deg Phi angle in degrees
 * @param s_params Input parameters for detection target position
 * @return True if device is in required position, false otherwise
 */
bool sensors_utils_ori_is_in_position(
    const int16_t i16_theta_deg, const int16_t i16_phi_deg,
    const ts_sensor_util_ori_det_params s_params);
// ======================| Get default argument values |======================
/**
 * @brief Get default arguments for sampling mode
 *
 * @note It does not set callback function!
 *
 * @param[out] ps_args Pointer to argument structure
 */
void sensors_utils_get_default_sampling_args(
    ts_sensor_util_sampling_args *ps_args);

/**
 * @brief Get default arguments for tap detection mode
 *
 * @note It does not set callback function!
 *
 * @param[out] ps_args Pointer to argument structure
 */
void sensors_utils_get_default_tap_detect(ts_sensor_util_tap_det_args *ps_args);

/**
 * @brief Get default arguments for orientation tracking mode
 *
 * @note It does not set callback function!
 *
 * @param[out] ps_args Pointer to argument structure
 */
void sensors_utils_get_default_ori_tracking(
    ts_sensor_util_ori_det_args *ps_args);
#endif  // __SENSORS_UTILS_H__
