# About

* Utilities for *sensors* module
* It offers high level processing data from sensors
* In order to be at least somehow optimized for embedded systems, it require
  some additional dependencies (mainly lightweight mathematics functionality)

# Main features

* Tap detection
* Orientation detection
* Periodic sampling raw data (can be used for post processing/machine learning)

# Example

* Example counts with connected accelerometer `MPU6050`. If any other supported accelerometer will be connected, it might work too, but some thresholds might be necessary to change.

